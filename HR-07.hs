fn n = reverse([1,2,3,4])
main = do
n <- readLn :: IO Int
print (fn(n))
