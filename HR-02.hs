import Control.Monad
main = do
  num <- readLn
  replicateM_ num $ putStrLn "Hello World"
